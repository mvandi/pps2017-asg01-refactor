package laterunner.physics;

import java.io.Serializable;
import java.util.Objects;

public class Vector2 implements Serializable {

    private static final long serialVersionUID = 1L;

    private double x;
    private double y;

    /**
     * @param x param to set x
     * @param y param to set y
     */
    public Vector2(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Translate the point.
     *
     * @param v the vector to translate the point
     * @return the point after the translation
     */
    public Vector2 sum(final Vector2 v) {
        return new Vector2(x + v.getX(), y + v.getY());
    }

    /**
     * Mul the vector by fact.
     *
     * @param fact param to multiply the vector
     * @return vector * fact
     */
    public Vector2 mul(final double fact) {
        return new Vector2(x * fact, y * fact);
    }

    /**
     * Get the x component.
     *
     * @return the x component
     */
    public double getX() {
        return x;
    }

    /**
     * Set the x component.
     *
     * @param x the new x component
     */
    public void setX(final double x) {
        this.x = x;
    }

    /**
     * Get the y component.
     *
     * @return the y component
     */
    public double getY() {
        return y;
    }

    /**
     * Set the y component.
     *
     * @param y the new y component
     */
    public void setY(final double y) {
        this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector2)) return false;
        final Vector2 other = (Vector2) o;
        return Double.compare(other.getX(), getX()) == 0
                && Double.compare(other.getY(), getY()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

}
