package laterunner.graphics;

import laterunner.core.GameEngine;
import laterunner.input.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;

import static laterunner.input.Commands.*;

/**
 * Scene Implementation.
 */
public class SceneImpl extends JFrame implements Scene, KeyListener {

    private static final long serialVersionUID = 1L;

    private static final int WIDTH = 960;
    private static final int HEIGHT = 720;
    private static final CardLayout CARD_LAYOUT = new CardLayout();
    private static final JPanel COUNTER_PANEL = new JPanel(CARD_LAYOUT);

    private final Road road;
    private final Controller controller;

    /**
     * Common Scene Constructor: instantiates all the panels and set Menu as first view.
     *
     * @param engine     instance of Controller class GameEngine
     * @param controller instance of Controller class Controller
     */
    public SceneImpl(final GameEngine engine, final Controller controller) {
        this.addKeyListener(this);
        this.controller = controller;
        road = new Road(engine);
        final Menu menu = new Menu(this.road, engine);
        final Market shop = new Market();
        final Statistics statistics = new Statistics();

        COUNTER_PANEL.add(road, "road");
        COUNTER_PANEL.add(menu, "menu");
        COUNTER_PANEL.add(shop, "shop");
        COUNTER_PANEL.add(statistics, "statistics");
        changePanel("menu");

        add(COUNTER_PANEL, BorderLayout.CENTER);
        setSize(WIDTH, HEIGHT);
        setTitle("Late Runner");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        requestFocusInWindow();
        setVisible(true);
    }

    @Override
    public void render() {
        try {
            SwingUtilities.invokeAndWait(this::repaint);
        } catch (final InterruptedException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void keyPressed(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.notifyCommand(moveRight());
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(moveLeft());
        }
    }

    @Override
    public void keyTyped(final KeyEvent e) {
    }

    @Override
    public void keyReleased(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(stop());
        }
    }

    @Override
    public Road getRoad() {
        return road;
    }

    /**
     * Switches between panels in order to show the desired one.
     *
     * @param name the name of the panel to show
     */
    public static void changePanel(final String name) {
        CARD_LAYOUT.show(COUNTER_PANEL, name);
    }

}