package laterunner.graphics;

/**
 * Defines all the graphics resources.
 */
public enum IconType {

    /**
     * Back picture.
     */
    BACK,

    /**
     * Background picture.
     */
    BACKGROUND,

    /**
     * Bus picture.
     */
    BUS,

    /**
     * Buy_Life picture.
     */
    BUY_LIFE,

    /**
     * Buy_Speed picture.
     */
    BUY_SPEED,

    /**
     * Car picture.
     */
    CAR,

    /**
     * Coin picture.
     */
    COIN,

    /**
     * Cross picture.
     */
    CROSS,

    /**
     * Heart picture.
     */
    HEART,

    /**
     * Jeep picture.
     */
    JEEP,

    /**
     * Menu picture.
     */
    MENU,

    /**
     * Motor-bike picture.
     */
    MOTORBIKE,

    /**
     * Play picture.
     */
    PLAY,

    /**
     * Quit picture.
     */
    QUIT,

    /**
     * Road picture.
     */
    ROAD,

    /**
     * Shop picture.
     */
    SHOP,

    /**
     * Statistics picture.
     */
    STATS

}
