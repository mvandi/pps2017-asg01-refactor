package laterunner.graphics;

import laterunner.core.GameEngine;
import laterunner.model.user.User;
import laterunner.model.vehicle.Car;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;
import laterunner.model.world.GameState;
import laterunner.physics.Vector2;

import javax.swing.*;
import java.awt.*;

/**
 * Road is the class that contains the real game. It displays all the vehicles and its elements are constantly repainted.
 */
public class Road extends PanelImpl implements Runnable {

    private static final long serialVersionUID = 1L;

    private static final float SIZE = 60;
    private static final int STEP = -96;
    private static final int LEFT_CROSS = 372;
    private static final int RIGHT_CROSS = 572;
    private static final int SPEED_FACTOR = 21;
    private static final JLabel LABEL = new JLabel();

    private static int y = STEP;

    private final Audio audio = new Audio();

    private final GameEngine engine;
    private GameState gameState;

    /**
     * Common Road constructor: sets the GameEngine.
     *
     * @param engine an instance of GameEngine class
     */
    public Road(final GameEngine engine) {
        this.engine = engine;
        final Font fontChalkDash = createFont(SIZE);
        LABEL.setFont(fontChalkDash);
        LABEL.setForeground(Color.BLACK);
        add(LABEL, BorderLayout.NORTH);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);

        drawImage(g, IconType.ROAD, new Vector2(0, 0));
        updateCross();
        drawImage(g, IconType.CROSS, new Vector2(LEFT_CROSS, y));
        drawImage(g, IconType.CROSS, new Vector2(RIGHT_CROSS, y));

        for (final Vehicle vehicle : gameState.getWorld().getSceneEntities()) {
            LABEL.setText(getText());

            if (vehicle instanceof Car) {
                drawImage(g, IconType.CAR, vehicle.getPosition());
            } else if (vehicle instanceof Obstacle) {
                drawImage(g, getPictureType(vehicle.getType()), vehicle.getPosition());
            }
        }
    }

    /**
     * Sets the GameState.
     *
     * @param gameState an instance of GameState class
     */
    public void setGameState(final GameState gameState) {
        this.gameState = gameState;
    }

    @Override
    public void run() {
        engine.mainLoop();
    }

    /**
     * Returns an instance of Audio class.
     *
     * @return an instance of Audio class
     */
    public Audio getAudio() {
        return audio;
    }

    private void updateCross() {
        y += SPEED_FACTOR;
        if (y >= 0) {
            y = STEP;
        }
    }

    private String getText() {
        String text = "<html><div style='text-align: center;'>";
        if (!engine.isSurvival()) {
            text += "Lives: " + User.getUser().getLives() + "&#160; &#160; &#160; &#160; &#160;";
        }
        text += "Score: " + gameState.getScore() + "</div></html>";
        return text;
    }

    private static IconType getPictureType(final VehicleType type) {
        switch (type) {
            case OBSTACLE_CAR:
                return IconType.JEEP;
            case BUS:
                return IconType.BUS;
            case MOTORBIKE:
                return IconType.MOTORBIKE;
            default:
                throw new IllegalStateException();
        }
    }

}