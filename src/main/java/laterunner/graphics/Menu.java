package laterunner.graphics;

import laterunner.core.GameEngine;
import laterunner.model.user.User;
import laterunner.physics.Vector2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;


/**
 * Menu is the class that displays the main menu and it's the first panel showed when the game starts.
 */
public class Menu extends PanelImpl {

    private static final long serialVersionUID = 1L;

    private static final int X = 72;
    private static final int Y = 268;
    private static final int INTERVAL = 10;
    private static final int LEVELS = 11;
    private static final int SURVIVAL = 11;

    private static int levelReached;
    private static final float SIZE = 48;

    private int level = 1;

    /**
     * Common Menu constructor. It has no layout and draws all the components relative to each other.
     *
     * @param road   an instance of Road class
     * @param engine an instance of GameEngine class
     */
    public Menu(final Road road, final GameEngine engine) {
        setLayout(null);
        final Font fontChalkDash = createFont(SIZE);
        updateLevel();

        //Combo box
        final JComboBox<String> levelBox = new JComboBox<>();
        levelBox.setFont(fontChalkDash);

        final DefaultListCellRenderer renderer = new DefaultListCellRenderer();
        renderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        levelBox.setRenderer(renderer);

        for (int n = 1; n <= LEVELS; n++) {
            if (n == SURVIVAL) {
                levelBox.addItem("S");
            } else {
                levelBox.addItem(Integer.toString(n));
            }
        }

        levelBox.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (e.getItemSelectable().getSelectedObjects()[0].equals("S")) {
                    this.level = SURVIVAL;
                } else {
                    this.level = Integer.parseInt((String) e.getItemSelectable().getSelectedObjects()[0]);
                }
            }
        });

        levelBox.setEditable(true);
        levelBox.setOpaque(false);
        ((JTextField) levelBox.getEditor().getEditorComponent()).setOpaque(false);
        final ImageIcon playPng = Icons.getIcon(IconType.PLAY);
        levelBox.setBounds(X + playPng.getIconWidth() + INTERVAL, Y, playPng.getIconHeight(), playPng.getIconHeight());
        add(levelBox);

        //Play button
        final JButton play = createButton(playPng);
        play.setBounds(X, Y, playPng.getIconWidth(), playPng.getIconHeight());

        play.addActionListener(e -> {
            if (this.level <= levelReached || this.level == SURVIVAL) {
                engine.setupLevel(this.level, 0);
                SceneImpl.changePanel("road");
                new Thread(road).start();
            } else {
                JOptionPane.showMessageDialog(this, "You have to unblock it!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        //Shop button
        final ImageIcon shopPng = Icons.getIcon(IconType.SHOP);
        final JButton shop = createButton(shopPng);
        shop.setBounds(X, play.getY() + play.getHeight() + INTERVAL,
                shopPng.getIconWidth(), shopPng.getIconHeight());
        shop.addActionListener(e -> SceneImpl.changePanel("shop"));

        //Statistics button
        final ImageIcon statsPng = Icons.getIcon(IconType.STATS);
        final JButton stats = createButton(statsPng);
        stats.setBounds(X, shop.getY() + shop.getHeight() + INTERVAL,
                statsPng.getIconWidth(), statsPng.getIconHeight());
        stats.addActionListener(e -> SceneImpl.changePanel("stats"));

        //Quit Button
        final ImageIcon quitPng = Icons.getIcon(IconType.QUIT);
        final JButton quit = createButton(quitPng);
        quit.setBounds(X, stats.getY() + stats.getHeight() + INTERVAL,
                quitPng.getIconWidth(), quitPng.getIconHeight());
        quit.addActionListener(e -> System.exit(0));

        add(play);
        add(shop);
        add(stats);
        add(quit);
    }

    /**
     * Updates the number of the last level reached once the player has unlocked it.
     */
    public static void updateLevel() {
        levelReached = User.getUser().getLevelReached();
    }

    @Override
    protected void paintComponent(final Graphics g) {
        drawImage(g, IconType.MENU, new Vector2(0, 0));
    }

}

