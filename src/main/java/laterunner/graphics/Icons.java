package laterunner.graphics;

import javax.swing.*;
import java.awt.*;
import java.util.EnumMap;

import static laterunner.graphics.IconType.*;


/**
 * Icons Implementation.
 */
public final class Icons {

    private static final EnumMap<IconType, String> PATHS = new EnumMap<>(IconType.class);
    private static final EnumMap<IconType, ImageIcon> ICONS = new EnumMap<>(IconType.class);
    private static final EnumMap<IconType, Image> IMAGES = new EnumMap<>(IconType.class);

    public static ImageIcon getIcon(final IconType icon) {
        return ICONS.get(icon);
    }

    public static Image getImage(final IconType icon) {
        return IMAGES.get(icon);
    }

    private static String getPath(final IconType icon) {
        return PATHS.get(icon);
    }

    private static ImageIcon getEIcon(final IconType icon) {
        return new ImageIcon(Icons.class.getResource(getPath(icon)));
    }

    private static Image getEImage(final IconType icon) {
        return new ImageIcon(Icons.class.getResource(getPath(icon))).getImage();
    }

    private Icons() {
    }

    static {
        PATHS.put(BACK, "/back.png");
        PATHS.put(BACKGROUND, "/background.jpg");
        PATHS.put(BUS, "/bus.png");
        PATHS.put(BUY_LIFE, "/buy_life.png");
        PATHS.put(BUY_SPEED, "/buy_speed.png");
        PATHS.put(CAR, "/car.png");
        PATHS.put(COIN, "/coin.png");
        PATHS.put(CROSS, "/cross.png");
        PATHS.put(HEART, "/heart.png");
        PATHS.put(JEEP, "/jeep.png");
        PATHS.put(MENU, "/menu.png");
        PATHS.put(MOTORBIKE, "/motorbike.png");
        PATHS.put(PLAY, "/play.png");
        PATHS.put(QUIT, "/quit.png");
        PATHS.put(ROAD, "/road.png");
        PATHS.put(SHOP, "/shop.png");
        PATHS.put(STATS, "/stats.png");

        for (final IconType type : IconType.values()) {
            ICONS.put(type, getEIcon(type));
            IMAGES.put(type, getEImage(type));
        }
    }

}