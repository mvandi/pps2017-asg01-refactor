package laterunner.graphics;

import javax.sound.sampled.*;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;

/**
 * Audio is an utility class that provides methods to play music during the game.
 */
public class Audio {

    private final Clip clip;

    /**
     * Common Audio constructor.
     */
    public Audio() {
        try {
            final URL url = getClass().getResource("/LateRunner_Theme.wav");
            final AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            clip = AudioSystem.getClip();
            clip.open(audioIn);
        } catch (final UnsupportedAudioFileException | LineUnavailableException e) {
            throw new RuntimeException(e);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Method to play audio.
     */
    public void play() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    /**
     * Method to stop audio.
     */
    public void stop() {
        clip.stop();
        clip.setFramePosition(0);
    }

}
