package laterunner.graphics;

import laterunner.physics.Vector2;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

/**
 * Panel Implementation.
 */
class PanelImpl extends JPanel implements Panel {

    private static final long serialVersionUID = 1L;

    JButton createButton(final ImageIcon img) {
        final JButton button = new JButton(img);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        return button;
    }

    Font createFont(final float size) {
        try (InputStream in = getClass().getResourceAsStream("/Digital Dot Roadsign.otf")) {
            final Font customFont = Font.createFont(Font.TRUETYPE_FONT, in)
                    .deriveFont(size);
            final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(customFont);
            return customFont;
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        } catch (final FontFormatException e) {
            throw new RuntimeException(e);
        }
    }

    static void drawImage(final Graphics g, final IconType type, final Vector2 position) {
        g.drawImage(Icons.getImage(type), (int) position.getX(),
                (int) position.getY(), null);
    }

}