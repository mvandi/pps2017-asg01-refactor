package laterunner.graphics;

import laterunner.model.user.User;
import laterunner.physics.Vector2;

import javax.swing.*;
import java.awt.*;

/**
 * Statistics class displays all the info about user's game experience.
 */
class Statistics extends PanelImpl {

    private static final long serialVersionUID = 1L;
    private static final float SIZE = 60;
    private static final JLabel LABEL = new JLabel();

    /**
     * Common Statistics constructor: sets the panel's layout and draws the components over the background.
     */
    Statistics() {
        this.setLayout(new BorderLayout());
        final Font fontChalkDash = createFont(SIZE);

        //Back button
        final JButton back = createButton(Icons.getIcon(IconType.BACK));
        back.addActionListener(e -> SceneImpl.changePanel("menu"));

        LABEL.setFont(fontChalkDash);
        LABEL.setForeground(Color.WHITE);

        final JPanel tmp = new JPanel();
        tmp.setOpaque(false);
        tmp.add(LABEL);
        this.add(tmp, BorderLayout.CENTER);
        this.add(back, BorderLayout.PAGE_END);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        drawImage(g, IconType.BACKGROUND, new Vector2(0, 0));
        final User.Statistics statistics = User.Statistics.getStatistics();
        String text = "<br>Games Played: " + statistics.getGamesPlayed() + "<br>"
                + "Lives Lost: " + statistics.getLostLives() + "<br>"
                + "Survival High Score: " + statistics.getSurvivalHighScore() + "<br>"
                + "Motorbike Hits: " + statistics.getMotorbikeHits() + "<br>"
                + "Obstacle Car Hits: " + statistics.getObstacleCarHits() + "<br>"
                + "Bus Hits: " + statistics.getTruckHits();
        LABEL.setText("<html><div style='text-align: center;'>" + text + "</div></html>");
    }

}
