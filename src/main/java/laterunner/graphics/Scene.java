package laterunner.graphics;

/**
 * Scene is the main JFrame that contains every panel.
 */
public interface Scene {

    /**
     * Invokes paintComponent.
     */
    void render();

    /**
     * Gets an instance of Road.
     *
     * @return an instance of Road class
     */
    Road getRoad();

}
