package laterunner.graphics;

import laterunner.model.shop.Shop;
import laterunner.model.user.User;
import laterunner.physics.Vector2;

import javax.swing.*;
import java.awt.*;

/**
 * Market class provides the possibility of buy speed and lives in order to move ahead in the game.
 */
class Market extends PanelImpl {

    private static final long serialVersionUID = 1L;

    private static final float SIZE = 48;
    private static final int MAX_SPEED = 12000;

    private final JLabel lifeCost;
    private final JLabel moneyCost;
    private final JButton life;
    private final JButton coin;

    /**
     * Common Market constructor. Sets the panel's layout and creates some buttons in order to display
     * every info about lives and money.
     */
    Market() {
        setLayout(new GridLayout(4, 2));
        final Font fontChalkDash = createFont(SIZE);
        Box boxL = Box.createHorizontalBox();
        Box boxS = Box.createHorizontalBox();

        //Back button
        final JButton back = createButton(Icons.getIcon(IconType.BACK));
        back.addActionListener(e -> SceneImpl.changePanel("menu"));

        //Buy life button
        final JButton buyLife = createButton(Icons.getIcon(IconType.BUY_LIFE));
        buyLife.addActionListener(e -> Shop.getShop().buyLife());

        //Buy speed button
        final JButton buySpeed = createButton(Icons.getIcon(IconType.BUY_SPEED));
        buySpeed.addActionListener(e -> Shop.getShop().buySpeed());

        //Life button
        life = createButton(Icons.getIcon(IconType.HEART));
        life.setText("" + User.getUser().getLives());
        life.setFont(fontChalkDash);
        life.setHorizontalTextPosition(SwingConstants.CENTER);

        //Coin button
        coin = createButton(Icons.getIcon(IconType.COIN));
        coin.setText("" + User.getUser().getMoney());
        coin.setFont(fontChalkDash);
        coin.setHorizontalTextPosition(SwingConstants.CENTER);

        final JLabel userLives = new JLabel("     Your lives:");
        userLives.setFont(fontChalkDash);
        userLives.setForeground(Color.WHITE);
        boxL.add(userLives);
        boxL.add(life);
        final JLabel userMoney = new JLabel("    Your money:");
        userMoney.setFont(fontChalkDash);
        userMoney.setForeground(Color.WHITE);
        boxS.add(userMoney);
        boxS.add(coin);

        //Life and Money costs
        lifeCost = new JLabel("         Life Cost: " + Shop.getShop().getLifeCost());
        lifeCost.setFont(fontChalkDash);
        lifeCost.setForeground(Color.WHITE);
        moneyCost = new JLabel("        Speed Cost: " + Shop.getShop().getSpeedCost());
        moneyCost.setFont(fontChalkDash);
        moneyCost.setForeground(Color.WHITE);

        add(boxL);
        add(boxS);
        add(lifeCost);
        add(moneyCost);
        add(buyLife);
        add(buySpeed);
        add(back);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        drawImage(g, IconType.BACKGROUND, new Vector2(0, 0));
        life.setText("" + User.getUser().getLives());
        final Shop shop = Shop.getShop();
        lifeCost.setText("         Life Cost: " + shop.getLifeCost());
        coin.setText("" + User.getUser().getMoney());
        if (shop.getSpeedCost() >= MAX_SPEED) {
            moneyCost.setText("        Speed Max");
        } else {
            moneyCost.setText("        Speed Cost: " + shop.getSpeedCost());
        }
    }

}
