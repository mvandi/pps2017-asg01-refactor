package laterunner.core;

import laterunner.graphics.IconType;
import laterunner.graphics.Icons;
import laterunner.model.vehicle.VehicleType;

/**
 * Dimensions class.
 */
public final class Dimensions {

    private static final int WIDTH_JEEP = 21;
    private static final int WIDTH_BUS = 20;
    private static final int WIDTH_MOTORBIKE = 15;
    private static final int WIDTH_CAR = 12;
    private static final int HEIGHT_JEEP = 21;
    private static final int HEIGHT_BUS = 26;
    private static final int HEIGHT_MOTORBIKE = 47;

    /**
     * Calculate the right vehicle's width.
     *
     * @param type the type of vehicle used to select the right one obstacle
     * @return width
     */

    public static double getVehicleWidth(final VehicleType type) {
        switch (type) {
            case OBSTACLE_CAR:
                return Icons.getIcon(IconType.JEEP).getIconWidth() - WIDTH_JEEP;
            case BUS:
                return Icons.getIcon(IconType.BUS).getIconWidth() - WIDTH_BUS;
            case MOTORBIKE:
                return Icons.getIcon(IconType.MOTORBIKE).getIconWidth() - WIDTH_MOTORBIKE;
            case USER_CAR:
                return Icons.getIcon(IconType.CAR).getIconWidth() - WIDTH_CAR;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Calculate the right vehicle's height.
     *
     * @param type the type of vehicle used to select the right one obstacle
     * @return height
     */
    public static double getVehicleHeight(final VehicleType type) {
        switch (type) {
            case OBSTACLE_CAR:
                return Icons.getIcon(IconType.JEEP).getIconHeight() - HEIGHT_JEEP;
            case BUS:
                return Icons.getIcon(IconType.BUS).getIconHeight() - HEIGHT_BUS;
            case MOTORBIKE:
                return Icons.getIcon(IconType.MOTORBIKE).getIconHeight() - HEIGHT_MOTORBIKE;
            case USER_CAR:
                return Icons.getIcon(IconType.CAR).getIconHeight();
            default:
                throw new IllegalStateException();
        }
    }

    private Dimensions() {
    }

}