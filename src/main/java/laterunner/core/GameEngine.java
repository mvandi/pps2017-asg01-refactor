package laterunner.core;

import laterunner.graphics.Menu;
import laterunner.graphics.Scene;
import laterunner.graphics.SceneImpl;
import laterunner.input.Command;
import laterunner.input.Controller;
import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEvent;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.saving.FileManager;
import laterunner.model.world.GameState;
import laterunner.model.world.GameStateImpl;
import laterunner.model.world.World;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Game's engine.
 */
public class GameEngine implements Controller, WorldEventListener {

    /*
     * 16 ms = 60 frames per second
     */
    private static final long PERIOD = 16;

    private GameState gameState;
    private Scene view;
    private boolean processingEnabled = false;
    private final BlockingQueue<Command> cmdQueue;
    private final List<WorldEvent> eventQueue;
    private int levelNumber;
    private boolean survival = false;

    /**
     * Instantiates game engine.
     */
    public GameEngine() {
        cmdQueue = new ArrayBlockingQueue<>(100);
        eventQueue = new LinkedList<>();
    }

    /**
     * Loads main settings and shows main menu.
     */
    public void gameInit() {
        FileManager.loadFromFile();
        view = new SceneImpl(this, this);
    }

    /**
     * Loads level.
     *
     * @param levelNumber level's number
     * @param score       level's score
     */
    public void setupLevel(final int levelNumber, final int score) {
        cmdQueue.clear();
        this.levelNumber = levelNumber;
        if (this.levelNumber > 10 && !(survival)) {
            survival = true;
            this.levelNumber = 1;
        } else if (levelNumber > 10) {
            this.levelNumber = 10;
        }
        gameState = new GameStateImpl(this, this.levelNumber, score);
        view.getRoad().setGameState(gameState);
    }

    /**
     * The loop which manages the game play.
     */
    public void mainLoop() {
        processingEnabled = true;
        view.getRoad().getAudio().play();
        long lastTime = System.currentTimeMillis();
        while (!gameState.isLevelCompleted() && !gameState.isSurvivalEnded()) {
            long current = System.currentTimeMillis();
            int elapsed = (int) (current - lastTime);
            processInput();
            updateGame(elapsed);
            render();
            waitForNextFrame(current);
            lastTime = current;
        }
        gameState.updateStatistics();
        endLevel();
    }

    private void endLevel() {
        if (!survival) {
            processingEnabled = false;
            view.getRoad().getAudio().stop();
            SceneImpl.changePanel("menu");
            Menu.updateLevel();
        } else {
            levelNumber++;
            setupLevel(levelNumber, gameState.getScore());
            new Thread(view.getRoad()).start();
        }
    }

    /**
     * Makes the main loop sleeping.
     *
     * @param currentTime time used to calculate how much to waits
     */
    private void waitForNextFrame(final long currentTime) {
        long deltaTime = System.currentTimeMillis() - currentTime;
        if (deltaTime < PERIOD) {
            try {
                Thread.sleep(PERIOD - deltaTime);
            } catch (final InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void processInput() {
        final Command command = cmdQueue.poll();
        if (command != null) {
            command.execute(gameState);
        }
    }

    private void updateGame(final int elapsed) {
        gameState.update(elapsed);
        checkEvents();
    }

    private void checkEvents() {
        final World world = gameState.getWorld();
        for (final WorldEvent event : eventQueue) {
            if (event instanceof ObstacleHitEvent) {
                final ObstacleHitEvent obstacleHit = (ObstacleHitEvent) event;
                if (survival) {
                    gameState.setSurvivalEnded(true);
                    survival = false;
                } else {
                    world.removeObstacle(obstacleHit.getObstacle());
                    gameState.decreaseScore(obstacleHit.getObstacle());
                }
            } else if (event instanceof BorderHitEvent) {
                gameState.decreaseScoreByBorder();
            }
        }
        if (eventQueue.isEmpty()) {
            gameState.increaseScore(1);
        }
        eventQueue.clear();
    }

    private void render() {
        view.render();
    }

    /**
     * Adds the command to the command queue.
     *
     * @param command Command executed
     */
    public void notifyCommand(final Command command) {
        if (processingEnabled) {
            cmdQueue.add(command);
        }
    }

    /**
     * Adds the event to the event queue.
     *
     * @param event Event happened
     */
    public void notifyEvent(final WorldEvent event) {
        eventQueue.add(event);
    }

    /**
     * Gets survival mode.
     *
     * @return true if survival.
     */
    public boolean isSurvival() {
        return survival;
    }

}
