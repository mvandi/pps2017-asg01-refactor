package laterunner.model.level;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Vector2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * The class in witch is implemented the method of levels' creation.
 */
final class LevelCreator {

    private static final int DISTANCE = 150;
    private static final int WIDTH_ROAD = 100;
    private static final int WIDTH_ROAD_PARALLEL = (int) (WIDTH_ROAD - Dimensions.getVehicleWidth(VehicleType.BUS));
    private static final int MOTORBIKE_GAP = 85;
    private static final int BUS_FRONT_GAP = 10;
    private static final int BUS_BACK_GAP = 83;
    private static final int CAR_FRONT_GAP = 55;
    private static final int CAR_BACK_GAP = 27;
    private static final int INITIAL_GAP = -400;
    private static final double QUEUE_RATE = 0.3;
    private static final double PARALLEL_RATE = 0.5;
    private static final int LEFT = 220;
    private static final int CENTER = 410;
    private static final int RIGHT = 600;

    private static final Random RANDOM = new Random();

    private double cont;
    private double gap;

    LevelCreator() {
        cont = 0;
        gap = INITIAL_GAP;
    }

    Level generateLevel(final LevelFeatures features) {
        final int distance = features.getDistance();
        final Vector2 speed = features.getSpeed();
        final List<VehicleType> obstacleTypes = features.getObstacleTypes();

        final List<Obstacle> obstacles = new LinkedList<>();

        int i = obstacleTypes.size();
        while (i > 0) {
            int v = RANDOM.nextInt(i);
            if (obstacles.isEmpty()) {
                Vector2 pos = new Vector2(getRandomPosition(), 0);
                obstacles.add(new Obstacle(obstacleTypes.get(v), pos, speed));
            } else {
                Vector2 pos = new Vector2(getRandomPosition(), obstacles.get(obstacles.size() - 1).getPosition().getY() + setY(distance, obstacleTypes.get(v), obstacles));
                if (obstacles.size() >= 3) {
                    changePos(obstacles, pos);
                }
                cont += (pos.getY() - obstacles.get(obstacles.size() - 1).getPosition().getY());
                obstacles.add(new Obstacle(obstacleTypes.get(v), pos, speed));
            }
            obstacleTypes.remove(v);
            i--;
        }
        gap -= cont;

        for (final Obstacle obstacle : obstacles) {
            obstacle.getPosition().setY(obstacle.getPosition().getY() + gap);
        }

        return new LevelImpl(obstacles);
    }

    private int getRandomPosition() {
        final int v = RANDOM.nextInt(3) + 1;
        switch (v) {
            case 1:
                return RANDOM.nextInt(WIDTH_ROAD) + LEFT;
            case 2:
                return RANDOM.nextInt(WIDTH_ROAD) + CENTER;
            default:
                return RANDOM.nextInt(WIDTH_ROAD) + RIGHT;
        }
    }

    private int setY(final int d, final VehicleType o, final List<Obstacle> list) {
        int distance = (int) (RANDOM.nextInt(d) + ((LevelCreator.DISTANCE) + Dimensions.getVehicleHeight(o)));
        if (o.equals(VehicleType.MOTORBIKE)) {
            distance += MOTORBIKE_GAP;
        }
        if (o.equals(VehicleType.BUS)) {
            distance += BUS_FRONT_GAP;
        }
        if (o.equals(VehicleType.OBSTACLE_CAR)) {
            distance += CAR_FRONT_GAP;
        }
        if (list.get(list.size() - 1).getType().equals(VehicleType.BUS)) {
            distance += BUS_BACK_GAP;
        }
        if (list.get(list.size() - 1).getType().equals(VehicleType.OBSTACLE_CAR)) {
            distance += CAR_BACK_GAP;
        }
        return distance;
    }

    private int getRange(final Vector2 position) {
        if (position.getX() >= LEFT && position.getX() <= WIDTH_ROAD + LEFT) {
            return LEFT;
        } else if (position.getX() >= CENTER && position.getX() <= WIDTH_ROAD + CENTER) {
            return CENTER;
        } else {
            return RIGHT;
        }
    }

    private boolean isInTheSameRange(final List<Obstacle> obstacles, final Vector2 pos, final int x) {
        return (getRange(obstacles.get(obstacles.size() - x).getPosition()) == getRange(pos));
    }

    private boolean checkPosition(final List<Obstacle> obstacles, final int x, final int range) {
        return getRange(obstacles.get(obstacles.size() - x).getPosition()) == range;
    }

    private void setParallelInCenter(final List<Obstacle> obstacles, final Vector2 position, final int first, final int second) {
        obstacles.get(obstacles.size() - 1).getPosition().setX(RANDOM.nextInt(WIDTH_ROAD_PARALLEL) + first);
        position.setX(RANDOM.nextInt(WIDTH_ROAD_PARALLEL) + second);
        position.setY(obstacles.get(obstacles.size() - 1).getPosition().getY());
    }

    private void setParallelInSide(final List<Obstacle> obstacles, final Vector2 pos, final int side) {
        pos.setX(RANDOM.nextInt(WIDTH_ROAD) + side);
        pos.setY(obstacles.get(obstacles.size() - 1).getPosition().getY());
    }

    private boolean checkEmptyLine(final List<Obstacle> obstacles, final Vector2 pos, final int first, final int second) {
        return getRange(pos) == first
                && checkPosition(obstacles, 1, second)
                && checkPosition(obstacles, 2, second)
                && checkPosition(obstacles, 3, first);
    }

    private boolean checkEmptyCenterLine(final List<Obstacle> obstacles, final int side) {
        return checkPosition(obstacles, 1, side)
                && checkPosition(obstacles, 2, side)
                && getRange(obstacles.get(obstacles.size() - 3).getPosition()) != CENTER;
    }

    private boolean checkTrianglePosition(final List<Obstacle> obstacles, final Vector2 position, final int first, final int second) {
        return getRange(position) == first
                && checkPosition(obstacles, 1, second)
                && checkPosition(obstacles, 2, first)
                && getRange(obstacles.get(obstacles.size() - 3).getPosition()) != second
                && !(obstacles.get(obstacles.size() - 1).getPosition().getY() == obstacles.get(obstacles.size() - 2).getPosition().getY());
    }

    private void checkDuplicateObstacle(final List<Obstacle> list, final Vector2 pos) {
        if (pos.getY() == list.get(list.size() - 1).getPosition().getY()
                && isInTheSameRange(list, pos, 1)) {
            pos.setY(pos.getY() + LevelCreator.DISTANCE);
        }
    }

    private void changePos(final List<Obstacle> obstacles, final Vector2 position) {
        if (isInTheSameRange(obstacles, position, 1) && RANDOM.nextDouble() > QUEUE_RATE) {
            double x = getRange(obstacles.get(obstacles.size() - 2).getPosition());
            if (isInTheSameRange(obstacles, position, 2)) {
                position.setX(getRandomPosition());

            } else if (x == LEFT && getRange(position) == CENTER || x == CENTER && getRange(position) == LEFT) {
                position.setX(RANDOM.nextInt(WIDTH_ROAD) + RIGHT);
            } else if (x == LEFT && getRange(position) == RIGHT || x == RIGHT && getRange(position) == LEFT) {
                position.setX(RANDOM.nextInt(WIDTH_ROAD) + CENTER);
            } else {
                position.setX(RANDOM.nextInt(WIDTH_ROAD) + LEFT);
            }
        }

        if (checkEmptyLine(obstacles, position, LEFT, RIGHT)) {
            setParallelInCenter(obstacles, position, RIGHT, CENTER);
            checkDuplicateObstacle(obstacles, position);
        }
        if (checkEmptyLine(obstacles, position, RIGHT, LEFT)) {
            setParallelInCenter(obstacles, position, LEFT, CENTER);
        }
        if (checkEmptyLine(obstacles, position, LEFT, CENTER) || checkEmptyLine(obstacles, position, CENTER, LEFT)) {
            position.setX(RANDOM.nextInt(WIDTH_ROAD) + RIGHT);
        }
        if (checkEmptyLine(obstacles, position, RIGHT, CENTER) || checkEmptyLine(obstacles, position, CENTER, RIGHT)) {
            position.setX(RANDOM.nextInt(WIDTH_ROAD) + LEFT);
        }

        if (checkTrianglePosition(obstacles, position, LEFT, RIGHT)) {
            setParallelInSide(obstacles, position, LEFT);
        }
        if (checkTrianglePosition(obstacles, position, RIGHT, LEFT)) {
            setParallelInSide(obstacles, position, RIGHT);
            checkDuplicateObstacle(obstacles, position);
        }

        if (checkTrianglePosition(obstacles, position, LEFT, CENTER)
                && RANDOM.nextDouble() > PARALLEL_RATE) {
            setParallelInCenter(obstacles, position, CENTER, LEFT);
            checkDuplicateObstacle(obstacles, position);
        }

        if (checkTrianglePosition(obstacles, position, RIGHT, CENTER)
                && RANDOM.nextDouble() > PARALLEL_RATE) {
            setParallelInCenter(obstacles, position, CENTER, RIGHT);
            checkDuplicateObstacle(obstacles, position);
        }
        if (checkEmptyCenterLine(obstacles, RIGHT)) {
            setParallelInCenter(obstacles, position, RIGHT, CENTER);
            checkDuplicateObstacle(obstacles, position);
        }
        if (checkEmptyCenterLine(obstacles, LEFT)) {
            setParallelInCenter(obstacles, position, LEFT, CENTER);
            checkDuplicateObstacle(obstacles, position);
        }
    }

}
