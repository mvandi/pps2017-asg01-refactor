package laterunner.model.level;

import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Vector2;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

final class LevelFeatures {

    private static final Random RANDOM = new Random();
    private static final double TRUCK_RATE = 0.03;
    private static final double CAR_RATE = 0.1;

    private final Vector2 speed;
    private final int distance;
    private final List<VehicleType> obstacleTypes;

    LevelFeatures(final int speed, final int distance, final int obstacleCount, final int levelNumber) {
        this.speed = new Vector2(0, speed);
        this.distance = distance;
        obstacleTypes = IntStream.range(0, obstacleCount)
                .map(i -> levelNumber)
                .mapToObj(this::selectVehicle)
                .collect(toList());
    }

    Vector2 getSpeed() {
        return speed;
    }

    int getDistance() {
        return distance;
    }

    List<VehicleType> getObstacleTypes() {
        return obstacleTypes;
    }

    private VehicleType selectVehicle(final int levelNum) {
        final double rand = RANDOM.nextDouble();
        if (rand <= TRUCK_RATE * levelNum) {
            return VehicleType.BUS;
        } else if (rand > TRUCK_RATE * levelNum && rand <= CAR_RATE * levelNum) {
            return VehicleType.OBSTACLE_CAR;
        } else {
            return VehicleType.MOTORBIKE;
        }
    }

}
