package laterunner.model.level;

import laterunner.model.vehicle.Obstacle;

import java.util.List;

/**
 * The interface in witch is defined the levels.
 */
public interface Level {

    /**
     * Get the level's obstacle list.
     *
     * @return the level's list
     */
    List<Obstacle> getObstacles();

}
