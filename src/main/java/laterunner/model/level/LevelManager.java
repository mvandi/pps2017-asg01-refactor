package laterunner.model.level;

/**
 * The class that set the levels' features.
 */
public class LevelManager {


    private final LevelCreator creator = new LevelCreator();

    public LevelManager() {
    }

    /**
     * Based on the level create the level.
     *
     * @param levelNumber the level to be played
     * @return the level
     */
    public Level getLevel(final int levelNumber) {
        final LevelFeatures features = getLevelFeatures(levelNumber);
        return creator.generateLevel(features);
    }

    private LevelFeatures getLevelFeatures(final int levelNumber) {
        LevelFeatures features;
        switch (levelNumber) {
            case 1:
                features = new LevelFeatures(300, 90, 50, levelNumber);
                break;
            case 2:
                features = new LevelFeatures(350, 75, 70, levelNumber);
                break;
            case 3:
                features = new LevelFeatures(400, 60, 90, levelNumber);
                break;
            case 4:
                features = new LevelFeatures(450, 50, 110, levelNumber);
                break;
            case 5:
                features = new LevelFeatures(500, 40, 120, levelNumber);
                break;
            case 6:
                features = new LevelFeatures(550, 40, 130, levelNumber);
                break;
            case 7:
                features = new LevelFeatures(600, 30, 140, levelNumber);
                break;
            case 8:
                features = new LevelFeatures(650, 20, 150, levelNumber);
                break;
            case 9:
                features = new LevelFeatures(700, 10, 175, levelNumber);
                break;
            case 10:
                features = new LevelFeatures(800, 1, 200, levelNumber);
                break;
            default:
                throw new IllegalStateException();
        }
        return features;
    }

}
