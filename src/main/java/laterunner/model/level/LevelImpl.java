package laterunner.model.level;

import laterunner.model.vehicle.Obstacle;

import java.util.LinkedList;
import java.util.List;

/**
 * The class in witch is implemented a obstacles.
 */
public class LevelImpl implements Level {

    private final List<Obstacle> obstacles;

    public LevelImpl(final List<Obstacle> obstacles) {
        this.obstacles = new LinkedList<>(obstacles);
    }

    @Override
    public List<Obstacle> getObstacles() {
        return obstacles;
    }

}
