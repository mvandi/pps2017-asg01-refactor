package laterunner.model.shop;

import laterunner.model.saving.FileManager;
import laterunner.model.user.User;

/**
 * The class in witch is implemented the Shop.
 */
public final class Shop {

    private static final int INITIAL_LIFE_COST = 1000;
    private static final int INITIAL_SPEED_COST = 3000;
    private static final double LIFE_MULTIPLIER = 1.5;
    private static final int SPEED_MULTIPLIER = 2;

    private int lifeCost;
    private int speedCost;

    private Shop() {
        this.lifeCost = INITIAL_LIFE_COST;
        this.speedCost = INITIAL_SPEED_COST;
    }

    /**
     * Get the only instance of Shop.
     *
     * @return the only instance of Shop
     */
    public static Shop getShop() {
        return Holder.INSTANCE;
    }

    /**
     * The method to reset the Shop features.
     */
    public void reset() {
        this.lifeCost = INITIAL_LIFE_COST;
        this.speedCost = INITIAL_SPEED_COST;
    }

    /**
     * The method in witch you can buy a life.
     */
    public void buyLife() {
        if (this.isLifePurchasable()) {
            final User user = User.getUser();
            user.setMoney(user.getMoney() - lifeCost);
            user.setLives(user.getLives() + 1);
            this.lifeCost = (int) (lifeCost * LIFE_MULTIPLIER);
            FileManager.saveToFile();
        }
    }

    /**
     * The method in witch you can increase your speed.
     */
    public void buySpeed() {
        if (this.isSpeedPurchasable()) {
            final User user = User.getUser();
            user.setMoney(user.getMoney() - speedCost);
            user.setSpeedMultiplier(user.getSpeedMultiplier() + 0.5);
            speedCost *= SPEED_MULTIPLIER;
            FileManager.saveToFile();
        }
    }

    /**
     * Return the actual life cost.
     *
     * @return the life cost
     */
    public int getLifeCost() {
        return lifeCost;
    }

    /**
     * Get the actual speed cost.
     *
     * @return the speed cost
     */
    public int getSpeedCost() {
        return speedCost;
    }

    /**
     * Set the life cost to the specified amount.
     *
     * @param lifeCost the new life cost
     */
    public void setLifeCost(final int lifeCost) {
        this.lifeCost = lifeCost;
    }

    /**
     * Set the speed cost to the specified amount.
     *
     * @param speedCost the new speed cost
     */
    public void setSpeedCost(final int speedCost) {
        this.speedCost = speedCost;
    }

    /**
     * Check if is possible buy a life.
     *
     * @return true if is possible buy a life
     */
    private boolean isLifePurchasable() {
        final User user = User.getUser();
        return user.getMoney() - lifeCost >= 0;
    }

    /**
     * Check if is possible buy speed.
     *
     * @return true if is possible buy speed
     */
    private boolean isSpeedPurchasable() {
        final User user = User.getUser();
        return user.getSpeedMultiplier() < SPEED_MULTIPLIER && user.getMoney() - speedCost >= 0;
    }

    private static final class Holder {
        private static final Shop INSTANCE = new Shop();
    }

}
