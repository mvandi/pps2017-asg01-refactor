package laterunner.model.user;

import laterunner.model.shop.Shop;

/**
 * Game user.
 */
public final class User {

    private static final int INITIAL_USER_LIFE = 3;
    private static final int STARTING_MONEY = 1000;
    private static final int STARTING_LEVEL = 1;
    private static final double STARTING_SPEED_MULTIPLIER = 1.0;

    private int money;
    private int levelReached;
    private double speedMultiplier;
    private int userLives;

    private User() {
        this.money = STARTING_MONEY;
        this.levelReached = STARTING_LEVEL;
        this.speedMultiplier = STARTING_SPEED_MULTIPLIER;
        this.userLives = INITIAL_USER_LIFE;
    }

    /**
     * Returns the only class' instance.
     *
     * @return user's instance
     */
    public static User getUser() {
        return Holder.INSTANCE;
    }

    /**
     * Returns user's money.
     *
     * @return user's money
     */
    public int getMoney() {
        return this.money;
    }

    /**
     * Sets user's money amount.
     *
     * @param amount new user's money amount
     */
    public void setMoney(final int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        } else {
            this.money = amount;
        }
    }

    /**
     * Returns user's level reached.
     *
     * @return user's level reached
     */
    public int getLevelReached() {
        return this.levelReached;
    }

    /**
     * Sets user's level reached.
     *
     * @param levelReached new user's level reached
     */
    public void setLevelReached(final int levelReached) {
        if (levelReached < 1) {
            throw new IllegalArgumentException();
        } else {
            this.levelReached = levelReached;
        }
    }

    /**
     * Returns user's multiplier.
     *
     * @return user's speed multiplier
     */
    public double getSpeedMultiplier() {
        return this.speedMultiplier;
    }

    /**
     * Sets user's speed multiplier.
     *
     * @param speedMultiplier new user's speed multiplier(1.0, 1.5 or 2.0)
     */
    public void setSpeedMultiplier(final double speedMultiplier) {
        if (speedMultiplier != STARTING_SPEED_MULTIPLIER && speedMultiplier != STARTING_SPEED_MULTIPLIER + 0.5
                && speedMultiplier != STARTING_SPEED_MULTIPLIER + 1.0) {
            throw new IllegalArgumentException();
        } else {
            this.speedMultiplier = speedMultiplier;
        }
    }

    /**
     * Returns user's lives.
     *
     * @return user's lives
     */
    public int getLives() {
        return this.userLives;
    }

    /**
     * Sets user's lives.
     *
     * @param lives new user's lives
     */
    public void setLives(final int lives) {
        if (lives < 0) {
            this.userLives = 0;
        } else {
            this.userLives = lives;
        }
    }

    /**
     * Resets user's fields and shop's references.
     */
    public void reset() {
        this.money = STARTING_MONEY;
        this.levelReached = STARTING_LEVEL;
        this.speedMultiplier = STARTING_SPEED_MULTIPLIER;
        this.userLives = INITIAL_USER_LIFE;
        Shop.getShop().reset();
    }

    private static final class Holder {
        private static final User INSTANCE = new User();
    }

    /**
     * User's statistic.
     */
    public static final class Statistics {

        private long motorbikeHits = 0;
        private long obstacleCarHits = 0;
        private long truckHits = 0;
        private long gamesPlayed = 0;
        private long lostLives = 0;
        private long survivalHighScore = 0;

        private Statistics() {
        }

        /**
         * Returns the only class' instance.
         *
         * @return user's statistic instance.
         */
        public static Statistics getStatistics() {
            return Holder.INSTANCE;
        }

        /**
         * Returns motorbike hits' number.
         *
         * @return motorbike hits' number
         */
        public long getMotorbikeHits() {
            return this.motorbikeHits;
        }

        /**
         * Sets new motorbike hits' number.
         *
         * @param mtrHits new motorbike hits' number
         */
        public void setMotorbikeHits(final long mtrHits) {
            if (mtrHits < this.motorbikeHits) {
                throw new IllegalStateException();
            } else {
                this.motorbikeHits = mtrHits;
            }
        }

        /**
         * Returns obstacle car hits' number.
         *
         * @return obstacle car hits' number
         */
        public long getObstacleCarHits() {
            return this.obstacleCarHits;
        }

        /**
         * Sets new obstacle car hits' number.
         *
         * @param obtCarHits new obstacle car hits' number
         */
        public void setObstacleCarHits(final long obtCarHits) {
            if (obtCarHits < this.obstacleCarHits) {
                throw new IllegalStateException();
            } else {
                this.obstacleCarHits = obtCarHits;
            }
        }

        /**
         * Returns truck hits' number.
         *
         * @return truck hits' number
         */
        public long getTruckHits() {
            return this.truckHits;
        }

        /**
         * Sets new truck hits' number.
         *
         * @param trcHits new truck hits' number
         */
        public void setTruckHits(final long trcHits) {
            if (trcHits < this.truckHits) {
                throw new IllegalStateException();
            } else {
                this.truckHits = trcHits;
            }
        }

        /**
         * Returns games played number.
         *
         * @return games played number
         */
        public long getGamesPlayed() {
            return this.gamesPlayed;
        }

        /**
         * Sets new games played number.
         *
         * @param gmsPlayed new games played number
         */
        public void setGamesPlayed(final long gmsPlayed) {
            if (gmsPlayed < this.gamesPlayed) {
                throw new IllegalStateException();
            } else {
                this.gamesPlayed = gmsPlayed;
            }
        }

        /**
         * Returns lost lives' number.
         *
         * @return lost lives' number
         */
        public long getLostLives() {
            return this.lostLives;
        }

        /**
         * Sets lost lives' number.
         *
         * @param lstLives new lost lives' number
         */
        public void setLostLives(final long lstLives) {
            if (lstLives < this.lostLives) {
                throw new IllegalStateException();
            } else {
                this.lostLives = lstLives;
            }
        }

        /**
         * Returns survival high score.
         *
         * @return survival high score number
         */
        public long getSurvivalHighScore() {
            return this.survivalHighScore;
        }

        /**
         * Sets survival high score number.
         *
         * @param highScore new survival high score number
         */
        public void setSurvivalHighScore(final long highScore) {
            if (highScore < this.survivalHighScore) {
                throw new IllegalStateException();
            } else {
                this.survivalHighScore = highScore;
            }
        }

        private static final class Holder {
            private static final Statistics INSTANCE = new Statistics();
        }

    }
}
