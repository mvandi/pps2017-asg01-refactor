package laterunner.model.vehicle;

import laterunner.model.collisions.BoundingBox;
import laterunner.model.collisions.ObstacleBoundingBox;
import laterunner.physics.Vector2;

/**
 * Game obstacle.
 */
public class Obstacle extends Vehicle {

    private static final int CAR_DAMAGE = 1000;
    private static final int CAR_LIFE_DAMAGE = 1;
    private static final int BUS_DAMAGE = 3000;
    private static final int BUS_LIFE_DAMAGE = 3;
    private static final int MOTORBIKE_DAMAGE = 2000;
    private static final int MOTORBIKE_LIFE_DAMAGE = 0;

    private int damage;
    private int lifeDamage;
    private BoundingBox bBox;

    /**
     * Instantiates a new Obstacle.
     *
     * @param type     obstacle's type
     * @param position obstacle starting position
     * @param speed    obstacle starting speed
     */
    public Obstacle(final VehicleType type, final Vector2 position, final Vector2 speed) {
        super(position, speed, type);

        switch (super.getType()) {
            case OBSTACLE_CAR:
                this.damage = CAR_DAMAGE;
                this.lifeDamage = CAR_LIFE_DAMAGE;
                break;
            case BUS:
                this.damage = BUS_DAMAGE;
                this.lifeDamage = BUS_LIFE_DAMAGE;
                break;
            case MOTORBIKE:
                this.damage = MOTORBIKE_DAMAGE;
                this.lifeDamage = MOTORBIKE_LIFE_DAMAGE;
                break;
            default:
                throw new IllegalArgumentException("Obstacle with user car feature. Nice try :)");
        }

        bBox = new ObstacleBoundingBox(getPosition().getX(), getType());
    }

    /**
     * Returns obstacle's damage.
     *
     * @return obstacle's damage
     */
    public int getDamage() {
        return this.damage;
    }

    /**
     * Returns obstacle's life damage.
     *
     * @return obstacle's life damage
     */
    public int getLifeDamage() {
        return this.lifeDamage;
    }

    /**
     * Returns obstacle's bounding box.
     *
     * @return obstacle's bounding box
     */
    public BoundingBox getBBox() {
        return this.bBox;
    }

    @Override
    public void setSpeed(final Vector2 speed) {
        super.setCheckedSpeed(new Vector2(0, speed.getY()));
    }

}
