package laterunner.model.vehicle;

import laterunner.physics.Vector2;

/**
 * User's Car.
 */
public class Car extends Vehicle {

    private static final int X_POS = 435;
    private static final int Y_POS = 510;

    /**
     * Instantiates a new Car.
     */
    public Car() {
        super(new Vector2(X_POS, Y_POS), new Vector2(0, 0), VehicleType.USER_CAR);
    }

    @Override
    public void setSpeed(final Vector2 speed) {
        setCheckedSpeed(new Vector2(speed.getX(), 0));
    }

}
