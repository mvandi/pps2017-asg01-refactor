package laterunner.model.vehicle;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderBoundingBox;
import laterunner.physics.Vector2;

/**
 * Template of a vehicle.
 */
public abstract class Vehicle {

    private static final double UPDATING_CONSTANT = 0.001;

    private Vector2 position;
    private Vector2 speed;
    private VehicleType type;

    /**
     * VehicleType' main constructor.
     *
     * @param position vehicle's initial position
     * @param speed    vehicle's initial speed
     * @param type     vehicle's type
     */
    Vehicle(final Vector2 position, final Vector2 speed, final VehicleType type) {
        if ((position.getX() < BorderBoundingBox.getBorderBoundingBox().getUpperLeft().getX()
                && position.getX() + Dimensions.getVehicleWidth(type) > BorderBoundingBox.getBorderBoundingBox().getBelowRight().getX())
                || (type == VehicleType.USER_CAR && speed.getY() != 0)
                || (type != VehicleType.USER_CAR && speed.getX() != 0)) {
            throw new IllegalArgumentException();
        } else {
            this.position = position;
            this.speed = speed;
            this.type = type;
        }
    }

    /**
     * Sets vehicle's position.
     *
     * @param position vehicle's new position
     */
    public void setPosition(final Vector2 position) {
        if (position.getX() < BorderBoundingBox.getBorderBoundingBox().getUpperLeft().getX()
                && position.getX() + Dimensions.getVehicleWidth(getType()) > BorderBoundingBox.getBorderBoundingBox().getBelowRight().getX()) {
            throw new IllegalArgumentException();
        } else {
            this.position = position;
        }
    }

    /**
     * Recalls the checking setter.
     *
     * @param speed unchecked vehicle's new speed
     */
    public abstract void setSpeed(final Vector2 speed);

    /**
     * Sets the vehicles's speed after checking it.
     *
     * @param speed checked vehicle's new speed
     */
    void setCheckedSpeed(final Vector2 speed) {
        this.speed = speed;
    }

    /**
     * Calculates new vehicle's position depending on the speed.
     *
     * @param elapsed time elapsed between two frames
     */
    public void updateState(final int elapsed) {
        position = position.sum(speed.mul(UPDATING_CONSTANT * elapsed));
    }

    /**
     * Returns vehicle's current position.
     *
     * @return vehicle's current position
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Returns vehicle's current speed.
     *
     * @return vehicle's current speed
     */
    public Vector2 getSpeed() {
        return speed;
    }

    /**
     * Returns vehicle's type.
     *
     * @return vehicle's type
     */
    public VehicleType getType() {
        return type;
    }

}
