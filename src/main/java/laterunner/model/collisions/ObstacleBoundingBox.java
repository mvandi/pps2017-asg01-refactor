package laterunner.model.collisions;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;

/**
 * Horizontal segment of the obstacles' occupied space.
 */
public class ObstacleBoundingBox implements BoundingBox {

    private final double xPos;
    private final double xPosSim;

    /**
     * Instantiates the horizontal segment of the obstacle's occupied space.
     *
     * @param xPos         left side of the obstacle
     * @param obstacleType type of the obstacle
     */
    public ObstacleBoundingBox(final double xPos, final VehicleType obstacleType) {
        this.xPos = xPos;
        this.xPosSim = this.xPos + Dimensions.getVehicleWidth(obstacleType);
    }

    @Override
    public boolean isCollidingWith(final Vehicle vehicle) {
        return vehicle.getPosition().getX() <= xPosSim
                && vehicle.getPosition().getX() + Dimensions.getVehicleWidth(VehicleType.USER_CAR) >= this.xPos;
    }

}
