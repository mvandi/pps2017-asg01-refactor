package laterunner.model.collisions;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Vector2;

/**
 * Square in which the car and obstacles can appear.
 */
public final class BorderBoundingBox implements BoundingBox {

    private static final Vector2 UPPER_LEFT = new Vector2(172, 0);
    private static final Vector2 BELOW_RIGHT = new Vector2(773, 511);

    /**
     * Returns the only class' instance.
     *
     * @return border bounding box
     */
    public static BorderBoundingBox getBorderBoundingBox() {
        return Holder.INSTANCE;
    }

    @Override
    public boolean isCollidingWith(final Vehicle vehicle) {
        boolean collision = false;
        if (vehicle.getPosition().getX() + Dimensions.getVehicleWidth(VehicleType.USER_CAR) > BELOW_RIGHT.getX()) {
            vehicle.setPosition(new Vector2(BELOW_RIGHT.getX()
                    - Dimensions.getVehicleWidth(VehicleType.USER_CAR) - 1,
                    vehicle.getPosition().getY()));
            collision = true;
        } else if (vehicle.getPosition().getX() < UPPER_LEFT.getX()) {
            vehicle.setPosition(new Vector2(UPPER_LEFT.getX() + 1, vehicle.getPosition().getY()));
            collision = true;
        }
        return collision;
    }

    /**
     * Returns box' upper left corner.
     *
     * @return box' upper left corner.
     */
    public Vector2 getUpperLeft() {
        return UPPER_LEFT;
    }

    /**
     * Returns box' below right corner.
     *
     * @return box' below right corner.
     */
    public Vector2 getBelowRight() {
        return BELOW_RIGHT;
    }

    /**
     * Instantiates the street bounding box.
     */
    private BorderBoundingBox() {
    }

    private static final class Holder {
        private static final BorderBoundingBox INSTANCE = new BorderBoundingBox();
    }

}
