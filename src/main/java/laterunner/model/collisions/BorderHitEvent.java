package laterunner.model.collisions;

/**
 * Represents the border hit's event.
 */
public class BorderHitEvent implements WorldEvent {

    /**
     * Instantiates a border event.
     */
    public BorderHitEvent() {
    }

}
