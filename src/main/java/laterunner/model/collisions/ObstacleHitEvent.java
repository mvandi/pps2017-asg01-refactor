package laterunner.model.collisions;

import laterunner.model.vehicle.Obstacle;

/**
 * Obstacle hit event.
 */
public class ObstacleHitEvent implements WorldEvent {

    private final Obstacle obstacle;

    /**
     * Instantiates a new obstacle hit event.
     *
     * @param obstacle obstacle hit
     */
    public ObstacleHitEvent(final Obstacle obstacle) {
        this.obstacle = obstacle;
    }

    /**
     * Returns the obstacle hit.
     *
     * @return obstacle hit
     */
    public Obstacle getObstacle() {
        return obstacle;
    }

}
