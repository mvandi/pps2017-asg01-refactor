package laterunner.model.world;

import laterunner.model.vehicle.Obstacle;

/**
 * The interface in which is defined the game's state.
 */
public interface GameState {

    /**
     * Return the GameState's world.
     *
     * @return the GameState's world
     */
    World getWorld();

    /**
     * Decrease the score by the damage of the obstacle.
     *
     * @param obstacle the Obstacle hit
     */
    void decreaseScore(final Obstacle obstacle);

    /**
     * Decrease the score by the damage of the border.
     */
    void decreaseScoreByBorder();

    /**
     * Increase score by i.
     *
     * @param i the value to increase the score
     */
    void increaseScore(final int i);

    /**
     * Get the level's score.
     *
     * @return the score
     */
    int getScore();

    /**
     * Update the world by elapsed.
     *
     * @param elapsedTime the time elapsed
     */
    void update(final int elapsedTime);

    /**
     * Check if the level is finished.
     *
     * @return true is the level is finished
     */
    boolean isLevelCompleted();

    /**
     * Return true is mood survival is finished.
     *
     * @return true is mood survival is finished
     */
    boolean isSurvivalEnded();

    /**
     * Set the mood survival at endSurvival.
     *
     * @param survivalEnded the parameter to set survival
     */
    void setSurvivalEnded(final boolean survivalEnded);

    /**
     * Update the User statistics.
     */
    void updateStatistics();

}