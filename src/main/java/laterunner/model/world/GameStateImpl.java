package laterunner.model.world;

import laterunner.model.collisions.BorderBoundingBox;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.saving.FileManager;
import laterunner.model.user.User;
import laterunner.model.vehicle.Obstacle;

/**
 * The class in which is implemented the game's state.
 */
public class GameStateImpl implements GameState {

    private final World world;
    private int score;
    private final int level;
    private boolean survivalEnded;

    /**
     * @param listener the listener to be set in World
     * @param level    the level to play
     * @param score    the initial score of the level
     */
    public GameStateImpl(final WorldEventListener listener, final int level, final int score) {
        this.level = level;
        this.survivalEnded = false;
        world = new WorldImpl(BorderBoundingBox.getBorderBoundingBox());
        world.generateLevel(this.level);
        world.setEventListener(listener);
        this.score = score;
    }

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public void decreaseScore(final Obstacle obstacle) {
        score -= obstacle.getDamage();
        if (this.score < 0) {
            this.score = 0;
        }
        final User user = User.getUser();
        user.setLives(user.getLives() - obstacle.getLifeDamage());
        final User.Statistics statistics = User.Statistics.getStatistics();
        statistics.setLostLives(statistics.getLostLives() + obstacle.getLifeDamage());
        switch (obstacle.getType()) {
            case OBSTACLE_CAR:
                statistics.setObstacleCarHits(statistics.getObstacleCarHits() + 1);
                break;
            case MOTORBIKE:
                statistics.setMotorbikeHits(statistics.getMotorbikeHits() + 1);
                break;
            case BUS:
                statistics.setTruckHits(statistics.getTruckHits() + 1);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public void decreaseScoreByBorder() {
        this.score -= world.getBorderDamage();
        if (this.score < 0) {
            this.score = 0;
        }
    }

    @Override
    public void increaseScore(final int i) {
        this.score += i;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void update(final int elapsedTime) {
        this.world.updateState(elapsedTime);
    }

    @Override
    public boolean isLevelCompleted() {
        return this.getWorld().getSceneEntities().size() == 1 || User.getUser().getLives() <= 0;
    }

    @Override
    public boolean isSurvivalEnded() {
        return survivalEnded;
    }

    @Override
    public void setSurvivalEnded(final boolean survivalEnded) {
        this.survivalEnded = survivalEnded;
    }

    @Override
    public void updateStatistics() {
        final User.Statistics statistics = User.Statistics.getStatistics();
        if (this.survivalEnded) {
            if (statistics.getSurvivalHighScore() < this.score) {
                statistics.setSurvivalHighScore(this.score);
            }
        } else {
            final User user = User.getUser();
            if (user.getLives() <= 0) {
                user.reset();
            } else {
                user.setMoney(user.getMoney() + this.score * this.level);
                if (this.level == user.getLevelReached()) {
                    user.setLevelReached(this.level + 1);
                }
            }
        }
        statistics.setGamesPlayed(statistics.getGamesPlayed() + 1);
        FileManager.saveToFile();
        FileManager.loadFromFile();
    }

}
