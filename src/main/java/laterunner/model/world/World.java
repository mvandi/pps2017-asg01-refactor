package laterunner.model.world;

import laterunner.model.collisions.WorldEventListener;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.Vehicle;

import java.util.List;

/**
 * The interface in witch is defined the world features.
 */
public interface World {

    /**
     * Set the WorldEventListener.
     *
     * @param listener World event listener
     */
    void setEventListener(final WorldEventListener listener);

    /**
     * Remove an obstacle from the obstacle list.
     *
     * @param obstacle Obstacle to be removed
     */
    void removeObstacle(final Obstacle obstacle);

    /**
     * Returns scene's entities' list.
     *
     * @return scene's entities' list
     */
    List<Vehicle> getSceneEntities();

    /**
     * Updates the world entities.
     *
     * @param elapsedTime Time elapsed
     */
    void updateState(final int elapsedTime);

    /**
     * Generates the level by the level's number.
     *
     * @param levelNumber Level's number
     */
    void generateLevel(final int levelNumber);

    /**
     * Get the User's vehicle.
     *
     * @return User's vehicle
     */
    Vehicle getUserVehicle();

    /**
     * Get the border damage.
     *
     * @return return the border damage
     */
    int getBorderDamage();

}