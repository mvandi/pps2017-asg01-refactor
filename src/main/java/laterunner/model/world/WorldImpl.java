package laterunner.model.world;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.BoundingBox;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.level.LevelManager;
import laterunner.model.vehicle.Car;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The class in witch is implemented the world features.
 */
public class WorldImpl implements World {

    private static final int LOWER_BORDER = 720;
    private static final int BORDER_DAMAGE = 500;

    private final List<Obstacle> obstacles;
    private final LevelManager levelManager;
    private Vehicle vehicle;
    private final BoundingBox mainBBox;
    private WorldEventListener listener;

    /**
     * @param bBox the bounding box to be set
     */
    public WorldImpl(final BoundingBox bBox) {
        obstacles = new LinkedList<>();
        levelManager = new LevelManager();
        mainBBox = bBox;
    }

    @Override
    public void setEventListener(final WorldEventListener listener) {
        this.listener = listener;
    }

    @Override
    public void removeObstacle(final Obstacle obstacle) {
        obstacles.remove(obstacle);
    }

    @Override
    public List<Vehicle> getSceneEntities() {
        List<Vehicle> entities = new ArrayList<>(obstacles);
        entities.add(vehicle);
        return entities;
    }

    @Override
    public void updateState(final int elapsedTime) {
        obstacles.removeIf(obstacle -> {
            obstacle.updateState(elapsedTime);
            return obstacle.getPosition().getY() > LOWER_BORDER;
        });
        vehicle.updateState(elapsedTime);
        checkBoundaries();
        checkCollisions();
    }

    @Override
    public void generateLevel(final int levelNumber) {
        setVehicle(new Car());
        obstacles.addAll(levelManager.getLevel(levelNumber).getObstacles());
    }

    @Override
    public Vehicle getUserVehicle() {
        return vehicle;
    }

    private void setVehicle(final Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    private void checkBoundaries() {
        if (mainBBox.isCollidingWith(getUserVehicle())) {
            listener.notifyEvent(new BorderHitEvent());
        }
    }

    private void checkCollisions() {
        obstacles.stream()
                .filter(o -> o.getPosition().getY() + Dimensions.getVehicleHeight(o.getType()) >= (vehicle.getPosition().getY() + 2))
                .filter(o -> o.getPosition().getY() <= vehicle.getPosition().getY() + Dimensions.getVehicleHeight(VehicleType.USER_CAR))
                .filter(o -> o.getBBox().isCollidingWith(getUserVehicle()))
                .map(ObstacleHitEvent::new)
                .forEach(listener::notifyEvent);
    }

    @Override
    public int getBorderDamage() {
        return BORDER_DAMAGE;
    }

}
