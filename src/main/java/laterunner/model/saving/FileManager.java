package laterunner.model.saving;

import laterunner.model.shop.Shop;
import laterunner.model.user.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class which contains functions to save and load user's info.
 */
public final class FileManager {

    private static final String FILE = "file_manager.txt";

    /**
     * Saves user's info from file.
     */
    public static void saveToFile() {
        try (final BufferedWriter writer = Files.newBufferedWriter(Paths.get(FILE))) {
            List<String> toFile = getFields();
            Iterator<String> it = toFile.iterator();
            while (it.hasNext()) {
                writer.write(it.next());
                it.remove();
                writer.newLine();
            }
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Loads user's info from file.
     */
    public static void loadFromFile() {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(FILE))) {
            final List<String> fields = br.lines().collect(Collectors.toList());
            setFields(fields);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static List<String> getFields() {
        List<String> fields = new ArrayList<>();
        fields.add("" + User.getUser().getMoney());
        fields.add("" + User.getUser().getLives());
        fields.add("" + User.getUser().getLevelReached());
        fields.add("" + User.getUser().getSpeedMultiplier());
        fields.add("" + User.Statistics.getStatistics().getGamesPlayed());
        fields.add("" + User.Statistics.getStatistics().getLostLives());
        fields.add("" + User.Statistics.getStatistics().getSurvivalHighScore());
        fields.add("" + User.Statistics.getStatistics().getMotorbikeHits());
        fields.add("" + User.Statistics.getStatistics().getObstacleCarHits());
        fields.add("" + User.Statistics.getStatistics().getTruckHits());
        fields.add("" + Shop.getShop().getLifeCost());
        fields.add("" + Shop.getShop().getSpeedCost());
        return fields;
    }

    private static void setFields(final List<String> fields) {
        Iterator<String> it = fields.iterator();
        User.getUser().setMoney(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setLives(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setLevelReached(Integer.parseInt(it.next()));
        it.remove();
        User.getUser().setSpeedMultiplier(Double.parseDouble(it.next()));
        it.remove();
        User.Statistics.getStatistics().setGamesPlayed(Long.parseLong(it.next()));
        it.remove();
        User.Statistics.getStatistics().setLostLives(Long.parseLong(it.next()));
        it.remove();
        User.Statistics.getStatistics().setSurvivalHighScore(Long.parseLong(it.next()));
        it.remove();
        User.Statistics.getStatistics().setMotorbikeHits(Long.parseLong(it.next()));
        it.remove();
        User.Statistics.getStatistics().setObstacleCarHits(Long.parseLong(it.next()));
        it.remove();
        User.Statistics.getStatistics().setTruckHits(Long.parseLong(it.next()));
        it.remove();
        Shop.getShop().setLifeCost(Integer.parseInt(it.next()));
        it.remove();
        Shop.getShop().setSpeedCost(Integer.parseInt(it.next()));
        it.remove();
    }

    private FileManager() {
    }

}
