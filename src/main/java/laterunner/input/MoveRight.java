package laterunner.input;

import laterunner.model.user.User;
import laterunner.model.vehicle.Vehicle;
import laterunner.physics.Vector2;

/**
 * Move left command class.
 */
public final class MoveRight extends AbstractMovementCommand {

    private static final int RIGHT_SPEED = 300;

    static MoveRight getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    protected Vector2 getSpeed()  {
        return new Vector2(RIGHT_SPEED * User.getUser().getSpeedMultiplier(), 0);
    }

    private MoveRight() {
    }

    private static final class Holder {
        private static final MoveRight INSTANCE = new MoveRight();
    }

}
