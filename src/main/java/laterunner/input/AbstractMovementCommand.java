package laterunner.input;

import laterunner.model.user.User;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.world.GameState;
import laterunner.physics.Vector2;

abstract class AbstractMovementCommand implements Command {

    AbstractMovementCommand() {
    }

    @Override
    public final void execute(final GameState gameState) {
        gameState.getWorld().getUserVehicle().setSpeed(getSpeed());
    }

    protected abstract Vector2 getSpeed();

}
