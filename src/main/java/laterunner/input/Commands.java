package laterunner.input;

public final class Commands {

    public static MoveLeft moveLeft() {
        return MoveLeft.getInstance();
    }

    public static MoveRight moveRight() {
        return MoveRight.getInstance();
    }

    public static Stop stop() {
        return Stop.getInstance();
    }

    private Commands() {
    }

}
