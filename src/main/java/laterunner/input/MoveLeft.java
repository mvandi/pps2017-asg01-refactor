package laterunner.input;

import laterunner.model.user.User;
import laterunner.model.vehicle.Vehicle;
import laterunner.physics.Vector2;

/**
 * Move left command class.
 */
public final class MoveLeft extends AbstractMovementCommand {

    private static final int LEFT_SPEED = -300;

    static MoveLeft getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    protected Vector2 getSpeed() {
        return new Vector2(LEFT_SPEED * User.getUser().getSpeedMultiplier(), 0);
    }

    private MoveLeft() {
    }

    private static final class Holder {
        private static final MoveLeft INSTANCE = new MoveLeft();
    }

}
