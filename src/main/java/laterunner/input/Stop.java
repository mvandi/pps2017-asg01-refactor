package laterunner.input;

import laterunner.model.vehicle.Vehicle;
import laterunner.physics.Vector2;

/**
 * Move left command class.
 */
public final class Stop extends AbstractMovementCommand {

    static Stop getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    protected Vector2 getSpeed()  {
        return new Vector2(0, 0);
    }

    private Stop() {
    }

    private static final class Holder {
        private static final Stop INSTANCE = new Stop();
    }

}
