package laterunner.model;

import laterunner.model.vehicle.Car;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Vector2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests Vehicle's method.
 */
class VehicleTest {

    private static final int X_POS = 435;
    private static final int Y_POS = 510;
    private static final int BUS_MAL = 3000;

    /**
     * Tests Vehicle's initial fields.
     */
    @Test
    void testInitialFields() {
        Car car = new Car();
        Vector2 pos = new Vector2(X_POS, Y_POS);
        assertEquals(car.getPosition(), pos);
        Obstacle ob = new Obstacle(VehicleType.BUS, new Vector2(X_POS, 1),
                new Vector2(0, 1));
        assertEquals(ob.getLifeDamage(), 3);
        assertEquals(ob.getDamage(), BUS_MAL);
        car.setSpeed(new Vector2(1, 1));
        assertEquals(car.getSpeed(), new Vector2(1, 0));
        car.setSpeed(new Vector2(1, 1));
        assertEquals(ob.getSpeed(), new Vector2(0, 1));
        assertEquals(car.getType(), VehicleType.USER_CAR);
        assertEquals(ob.getType(), VehicleType.BUS);
    }

}
