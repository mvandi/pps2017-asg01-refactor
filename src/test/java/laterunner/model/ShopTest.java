package laterunner.model;

import laterunner.model.shop.Shop;
import laterunner.model.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Shop tester.
 */
class ShopTest {

    private static final Shop SHOP = Shop.getShop();
    private static final User USER = User.getUser();

    /**
     * Test the correct function of the class Shop.
     */
    @Test
    void testInitialFields() {
        USER.reset();
        SHOP.buyLife();
        assertEquals(SHOP.getLifeCost(), 1500);
        USER.setMoney(USER.getMoney() + 5000);
        SHOP.buyLife();
        assertEquals(SHOP.getLifeCost(), 2250);
        SHOP.buySpeed();
        assertEquals(SHOP.getSpeedCost(), 6000);
        USER.reset();
        SHOP.reset();
        assertEquals(SHOP.getLifeCost(), 1000);
        assertEquals(SHOP.getSpeedCost(), 3000);
        USER.setMoney(USER.getMoney() + 21000);
        SHOP.buySpeed();
        SHOP.buySpeed();
        assertEquals(SHOP.getSpeedCost(), 12000);
        USER.reset();
        SHOP.reset();
        SHOP.buyLife();
        SHOP.buyLife();
        assertEquals(User.getUser().getLives(), 4);
        USER.reset();
        SHOP.reset();
        SHOP.buySpeed();
        assertEquals(1.0, User.getUser().getSpeedMultiplier(), 0.0);
        USER.reset();
        USER.setMoney(USER.getMoney() + 21000);
        SHOP.reset();
        SHOP.buySpeed();
        SHOP.buySpeed();
        SHOP.buySpeed();
        assertEquals(2.0, User.getUser().getSpeedMultiplier(), 0.0);
    }

}
