package laterunner.model;

import laterunner.model.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests User's methods.
 */
class UserTest {

    private static final User USER = User.getUser();
    private static final double BAD_DOUBLE = 0.9;

    /**
     * Tests User's initial fields.
     */
    @Test
    void testInitialFields() {
        USER.reset();
        assertEquals(USER.getLevelReached(), 1);
        assertEquals(USER.getMoney(), 1000);
        assertEquals(1.0, USER.getSpeedMultiplier(), 0.0);
        assertEquals(USER.getLives(), 3);
        USER.setLives(-1);
        assertEquals(USER.getLives(), 0);
    }

    /**
     * Tests if methods throw exceptions on particular situations.
     */
    @Test
    void checkException() {
        assertThrows(IllegalArgumentException.class, () -> USER.setLevelReached(-1));

        assertThrows(IllegalArgumentException.class, () -> USER.setMoney(-1));

        assertThrows(IllegalArgumentException.class, () -> USER.setSpeedMultiplier(BAD_DOUBLE));

        assertThrows(IllegalStateException.class, () -> User.Statistics.getStatistics().setGamesPlayed(-1));
    }

}
