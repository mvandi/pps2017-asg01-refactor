package laterunner.model;

import laterunner.model.level.LevelManager;
import laterunner.model.vehicle.Obstacle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test the creation of level.
 */
class LevelTest {

    private final LevelManager manager = new LevelManager();
    private final List<Obstacle> obstacles = new LinkedList<>();
    private static final int DISTANCE = 150;

    @AfterEach
    void tearDown() {
        obstacles.clear();
    }

    /**
     * Test the correct function of the creation's algorithm.
     */
    @Test
    void testLevelOne() {
        obstacles.addAll(manager.getLevel(1).getObstacles());
        assertEquals(obstacles.size(), 50);
        for (Obstacle o : obstacles) {
            assertEquals(300, o.getSpeed().getY(), 0.0);
            assertObstacle(o);
        }

        assertEquals(obstacles.size(), 200);
        for (Obstacle o : obstacles) {
            assertEquals(800, o.getSpeed().getY(), 0.0);
            assertObstacle(o);
        }
    }

    @Test
    void testLevelTen() {
        obstacles.addAll(manager.getLevel(10).getObstacles());
        assertEquals(obstacles.size(), 200);
        for (Obstacle o : obstacles) {
            assertEquals(800, o.getSpeed().getY(), 0.0);
            assertObstacle(o);
        }
    }

    private void assertObstacle(final Obstacle obstacle) {
        if (!obstacles.get(0).equals(obstacle)) {
            final int i = obstacles.indexOf(obstacle);
            Obstacle obj = obstacles.get(i - 1);
            if (obstacle.getPosition().getY() != obj.getPosition().getY()) {
                assertTrue((obstacle.getPosition().getY() - obj.getPosition().getY()) >= DISTANCE);
            } else {
                assertNotEquals(obstacle.getPosition().getX(), obj.getPosition().getX());
            }
        }
    }

}
